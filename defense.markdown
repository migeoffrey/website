---
layout: home
title: "PhD Defense"

---

<div id ="welcome">
	<p>
		It took place on <b>December 11, 2020</b> at <b>2 pm</b>. <br>
	</p>
</div>


<script type="application/json" id="config">
{
  "roomName": "soutenance_Sophia-Karpenko"
}
</script>


<style>
  iframe { border-radius: .5rem; }
  .jitsi-loaded .loading { display: none; }
</style>


<div id="meet">
  <p class="loading">Loading ...</p>
</div>

<script src="https://rdv3.rendez-vous.renater.fr/external_api.js" X-crossorigin="anonymous"></script>


<script X-type="module">
  const CFG = JSON.parse(document.querySelector('script#config').textContent);

  const domain = 'rdv3.rendez-vous.renater.fr';
  const options = {
    roomName: CFG.roomName, // 'PickAnAppropriateMeetingNameHere',
    width: '100%', // 700,
    height: 700,
    parentNode: document.querySelector('#meet'),
    userInfo: CFG.userInfo,

    invitees_X: [ /* {
      email: 'ano@nymous.fr',
      name: 'Anonymous'
    } */ ],
    
    onload: ev => {
      const URL = ev.target.src;
    
      console.warn('> Jitsi loaded:', URL, ev);
    
      document.body.classList.add('jitsi-loaded');
    }
  };

  const jitsi = new JitsiMeetExternalAPI(domain, options);

  jitsi.addEventListener('incomingMessage', ev => console.warn('> Incoming:', ev));
  jitsi.addEventListener('outgoingMessage', ev => console.warn('> Outgoing:', ev));

</script>

Direct link to the meeting: <br>
<a href='https://rdv3.rendez-vous.renater.fr/soutenance_Sophia-Karpenko'>rdv3.rendez-vous.renater.fr/soutenance_Sophia-Karpenko</a><br>

<div style="padding-top:30px; text-align:right; font-size: x-small;"><img src="/images/jury.jpg" alt="Tove Jansson" width = 700px
		/><br><a href="http://www.fairyroom.ru/?p=33587">Tove Jansson</a></div>

---

Sophia / 01-Dec-2020. <br>
<br>
Code adapted from: <br>

 * https://gist.github.com/nfreear/3a0fbaf0566e8a332dd3e7e7d4699794 <br>
 <br>
 * https://jitsi.org/api/ <br>
 * https://jitsi.github.io/handbook/docs/dev-guide/dev-guide-iframe#api <br>


