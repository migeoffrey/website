---
layout: post
title:  "Liste non-exhaustive de réalisatrices de cinéma"
date: 2021-01-11 16:55:08 +0200
categories: post
permalink: "/realisatrices-cine/"

---

[Liste wikipédia](https://en.wikipedia.org/wiki/List_of_female_film_and_television_directors)

Par ordre alphabétique (nom).

- Al-Mansour Haifa
- Chytilová Věra 
- Farrokhzad Forough
- Gamze Ergüven Deniz
- Hammer Barbara
- Rungano Nyoni
- Satrapi Marjane
- Sciamma Céline
- Wachowski Lana
- Wachowski Lilly
