---

layout: post
title:  "Notes de lecture - À bas le patriarcat"
date:   2020-06-10 19:59:08 +0200
categories: post
permalink: "/antipatriarcat-communiste-libertaire/"

---

# Un regard communiste libertaire



<img src="https://editionsacratie.files.wordpress.com/2019/08/couv-copie.jpg" alt="Couv Vanina" width="150"
	title="Couverture Vanina" align="left"
	style="border:10px solid white"
	/>Notes de lecture de [*À bas le patriarcat*](https://editionsacratie.com/a-bas-le-patriarcat-un-point-de-vue-communiste-libertaire-vanina/) de **Vanina**

**Commentaire général :**

C'est un court texte, qui s'adresse à des convaincu.e.s. Il utilise beaucoup de raccourcis, peu de données, n'est presque pas sourcé et beaucoup de concepts sont considérés comme acquis.

Il est néanmoins intéressant dans sa critique du phénomène #MeToo et le rejet de l'État comme médiateur ou intermédiaire pour régler les questions et revendications féministes.









## Commentaire du phénomène #MeToo

Le phénomène #MeToo est présenté comme un non-mouvement par l'absence d'une organisation militante, de revendications politiques claires. Son but était de briser l'omerta — le silence — autour du harcèlement et violences commis dans l'espace public. Ce phénomène n'avait en général pas pour but de rendre justice, de réparer, de rétribuer, ni de venger (les délations ayant été très rares).

Et il est important de noter que le hashtag dénonçait surtout les agressions subies **dans l'espace public** (dans la rue ou au travail). Sauf que c'est dans l'espace privé qu'ont lieu la plupart des violences sexuelles et viols (80-90% des cas). L'image de l'inconnu violeur planqué dans une ruelle est donc très chouette pour renforcer les mesures de contrôle social, mais est très, très loin de la réalité de la majorité des cas.

### Dénonciation du harcèlement sur le lieu de travail

Elle tient sans doute au fait que les femmes sont de plus en plus nombreuses dans des secteurs qui, il y a encore quelques années, étaient exclusivement masculins. Il faut donc qu'elles apprennent à les **(ré)investir**. Puisqu'aujourd'hui le salariat/entreprenariat féminin est bien vu, même encouragé — tant qu'elles veulent bien jouer le jeu — on (y compris Macron) se bat donc contre le harcèlement sur les lieux de travail. On trouve donc aujourd'hui des femmes en haut poste à qui il arrive de **militer pour l'égalité des salaires et contre le harcèlement**. En revanche elles bataillent **sans doute moins contre les rapports hiérarchiques, contre l'ordre établi ou les abus de pouvoir**. Parce qu'elles en profitent aussi.

La parité dans les institutions ne va faire qu'intégrer les femmes aux règles du jeu capitalistes donc patriarcales. On doit certes combattre les inégalités salariales à poste égal, mais surtout repenser la distribution des deux sexes au sein des postes à bas/haut salaire. Repenser la valorisation de certains métiers par rapport à d'autres — pourquoi la recherche de profit prime sur l'éducation, sur la recherche ? Pourquoi l'utilité publique n'est pas reflétée dans les salaires et les moyens offerts (comme ça a été vivement discuté lors de cette pandémie) ? </br> La présence de femmes dans de plus en plus de corps de métiers est due à une évolution économique et sociale qui fait que leur main d'œuvre est elle aussi requise, une famille ne survit plus que rarement avec un seul salaire et les femmes n'ont plus de raison de dépendre financièrement d'un homme. 

Le partage du pouvoir économique et politique semble donc admissible, mais en finir avec le patriarcat est une autre histoire. L'**antipatriarcat** ne se résume en effet pas à une volonté d'intégration des sphères de pouvoir, et de jouer le jeu. Il veut redistribuer les cartes et **changer les règles du jeu**. 

Remarquons qu'**une agression (notamment un viol), est perçue différemment** si la femme qui en est victime appartient à la haute — elle est respectable parce que bourge et riche — ou si elle ne l'est pas parce que pauvre. Ceci a d'ailleurs eu cours de tout temps ces derniers siècles (noble vs paysanne, bourgeoise vs ouvrière...). </br> On observe donc cette **intersectionnalité** des luttes des classes et du mouvement des femmes : une femme, qu'elle soit riche ou pauvre aura de grandes chances de subir des agressions à caractère sexuel, mais pas de la même façon si elle est riche ou pauvre. Ni dans les chances, ni dans la prise en charge, ni dans l'indignation publique. </br> Il ne faut cependant pas tomber dans l'écueil de la lutte des classes des années 1970, où la lutte féministe a été rejetée au second plan par rapport à la lutte des classes. Se sentant exclues, les féministes sont donc souvent parties des différents mouvements. On les retrouvera cependant plus tard dans les sections luttes des classes du MLF par exemple. 

## Le rôle de l'État

### Institutionnalisation du féminisme

Pour canaliser le mouvement social en rupture avec l'ordre (le patriarcat), en 1974 a été créé un secrétariat d'État à la condition féminine. Cela a permis de focaliser du combat contre un sexisme et non contre le patriarcat. Le mouvement des femmes, après s'être essoufflé a été récupéré.

Depuis, et si on prend l'exemple récent du nouveau gouvernement, des engagements ont été pris... genre en 2017 pour réduire les violences sexistes. Ces mesures recouvraient l'**éducation**, l'**accompagnement des victimes** (unités spécialisées dans les hôpitaux, discussions) et surtout en matière de **répression** (création du délit d'outrage sexiste).

Ces **engagement n'ont pas été tenus** surtout par manque de budget (secrétariat égalité femmes hommes a subi une hausse vertigineuse de son budget de 1% et représente 30 millions d'euros aujourd'hui, soit 0.006% du budget total de l'État). Aujourd'hui on compte surtout sur les assos et sur leurs **bénévoles** pour faire le boulot de sensibilisation, d'accompagnement, de prise en charge des victimes de violences.

Et surtout ces engagements **sont des mesures qui existaient déjà** et se sont avérées **inefficaces**. À l'école, on a droit à 3 séances par an d'éducation à la sexualité — en quoi est-ce répondre à l'impératif d'égalité et de respect mutuel entre sexes ? La répression accrue des violences à caractère sexuel a été mise en place, mais les plaintes pour viol sont *très* souvent classées sans suite... alors ne parlons même pas du harcèlement de rue (faute de preuves, de témoins, ou de connaître l'identité du harceleur).

Ce que Macron essaye de rattraper symboliquement en terme d'égalité hommes-femmes, il le brise de toute façon dans sa politique économique et sociale. L'exemple pris est celui de la réforme du code de travail, qui est particulièrement néfaste pour les femmes (flexibilité de l'emploi difficile pour les familles monoparentales (85% femmes), plus d'obligation pour les employeurs de mener des négociations pour l'égalité pro, données pas forcément publiques...)

Il y a deux problèmes majeurs à vouloir l'intervention de l'État sur des sujets de violences sexuelles ou sexistes. C'est d'une part le **renforcement de son appareil répressif** (donc patriarcal et capitaliste) et d'autre part le **maintien de la subordination des femmes**. Il faut donc faire en sorte de déléguer le moins possible notre pouvoir et notre marge de manœuvre à l'État et aux institutions (Linda Gordon, 1981).

### Judiciarisation

Le dilemme du procès... 

#### Faut-il emprisonner les violeurs ?

Le viol n'est pas un acte rare ou aberrant, il s'inscrit dans la continuité des codes sociaux endossés par les sexes et des rapports qui en découlent. C'est la **culture du viol**. Le viol est une maladie sociale

De fait, y répondre avec l'institution judiciaire (le combat de Gisèle Halimi à partir de 1975) qui est là pour protéger la propriété privée et l'ordre établi est peu effectif. L'institution judiciaire privilégie les privilégiés. La guérison par l'enfermement est un non-sens (ça ne marche pas), et permet juste de reporter sur l'agresseur la responsabilité de la société. On ne s'intéresse pas vraiment aux femmes mais plus à l'ordre établi : si un mari viole sa femme on s'en offusque moins que quand c'est un intrus qui use des prérogatives de ce mari. 

Ce qui peut être intéressant, c'est transformer le procès en tribune contre l'ordre établi : ne pas traiter le violeur comme bouc émissaire, comme cas particulier, mais attaquer le système, l'idéologie dominante. Demander une justice et non une vengeance. 

Mais *in fine*, la seule réponse possible ne peut qu'être l'éducation et un changement idéologique.

#### Libération et non répression sexuelle 

Le harcèlement sexuel est inhérent à une culture où l'homme doit être actif, entreprenant, insistant dans la séduction. La femme, au contraire, passive, soumise, contrainte. Les jeux de séduction sont souvent construits sur ce schéma (il arrive alors de dire non alors qu'on pense oui, mentionne Linda Gordon en 1981). </br> Et donc combattre le harcèlement sexuel est difficile sans être antisexe (Linda Gordon, 1981), tellement les deux sont intriqués. Il faut cependant essayer de faire la part des choses et ne pas se figer dans la peur de la fin de la drague ou de la séduction, comme l'a illustré éloquemment une certaine tribune sur "La liberté d'importuner" d'une figure féminine de l'anti-féminisime à la française. Le féminisme n'équivaut pas à puritanisme. 

Le libertin Choderlos de Laclos écrivait dans *De l'éducation des femmes*

> Venez apprendre comment, nées compagnes de l'homme, vous êtes devenues son esclave ; comment, tombées dans cet état abject, vous êtes parvenues à vous y plaire, à le regarder comme votre état naturel. (1)

Il ne s'agit pas de créer une nouvelle "norme" des rapports de séduction, mais au contraire en créer une multitude en rejetant le conformisme (la sexualité qu'on voit à l'écran, réalisée par des Weinstein).

Pour que cesse le harcèlement de rue, il faut se réapproprier l'espace public de façon autonome. La non-mixité, même si elle n'est pas un but en soi, peut être utile à cette fin (en manifestation, en groupe de discussion par exemple), car les participantes ont un vécu commun bien que divers.

---

(1) Un extrait plus long :

> O femmes ! Approchez et venez m'entendre. Que votre  curiosité, dirigée une fois sur des objets utiles, contemple les  avantages que vous avait donnés la nature et que la société vous a  ravis. Venez apprendre comment, nées compagnes de l'homme, vous êtes  devenues son esclave ; comment, tombées dans cet état abject, vous êtes  parvenues à vous y plaire, à le regarder comme votre état naturel ;  comment enfin, dégradées de plus en plus par une longue habitude de  l'esclavage, vous en avez préféré les vices avilissants mais commodes  aux vertus plus pénibles d'un être libre et respectable. Si ce tableau  fidèlement tracé vous laisse de sang-froid, si vous pouvez le considérer sans émotion, retournez à vos occupations futiles. Le mal est sans  remède, les vices se sont changés en mœurs. Mais si au récit de vos  malheurs et de vos pertes, vous rougissez de honte et de colère, si des  larmes d'indignation s'échappent de vos yeux, si vous brûlez du noble  désir de ressaisir vos avantages, de rentrer dans la plénitude de votre  être, ne nous laissez plus abuser par de trompeuses promesses,  n'attendez point les secours des hommes auteurs de vos maux : ils n'ont  ni la volonté, ni la puissance de les finir, et comment pourraient-ils  vouloir former des femmes devant lesquelles ils seraient forcés de  rougir ? Apprenez qu'on ne sort de l'esclavage que par une grande  révolution. Cette révolution est-elle possible ? C'est à vous seules à  le dire puisqu'elle dépend de votre courage. Est-elle vraisemblable ? Je me tais sur cette question ; mais jusqu'à ce qu'elle soit arrivée, et  tant que les hommes régleront votre sort, je serai autorisé à dire, et  il me sera facile de prouver qu'il n'est aucun moyen de perfectionner  l'éducation des femmes.