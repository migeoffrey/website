---
layout: post
title:  "Références supplémentaires pour les meufs vénères"
date:   2020-04-12 23:15:08 +0200
categories: post
permalink: "/refs-meufs-veneres/"
---

<div class="img-with-text">
<img src="/images/10.jpg" alt=""
	title=""
	/>
	<p style="text-align:right">
		<font size="2">
			<i>Illustration <a href="http://www.fairyroom.ru/?p=14904">Petr Baguine</a></i>.
		</font>
	</p>
</div>

-  Pour un exemple de débats au sein du mouvement au début des années 2000 : [«Trouble dans les féminismes : la parité, et après ?»](https://www.cairn.info/revue-mouvements-2007-2-page-130.htm), Mouvements, 2007/2 (n° 50), p. 130-142.

> Là où une partie des féministes célèbrent des victoires, d’autres déplorent des erreurs lourdes de conséquences. Depuis ses premiers numéros, Mouvements s’est régulièrement fait l’écho de ces débats ; il a paru pertinent de chercher à recueillir aujourd’hui, auprès de différentes composantes de ce, ou plutôt de ces féminismes en France, les éléments d’un bilan de cette décennie. Mouvements a expérimenté une formule inédite venue remplacer la « table ronde » habituelle. Une vingtaine d’associations, de militantes et de chercheuses féministes ont été contactées, et il leur a été demandé de répondre de manière très concise à la pourtant difficile et volontairement ouverte question suivante : « 1997-2007 : au cours de cette dernière décennie, quelles ont été selon vous l’avancée la plus importante du féminisme et sa défaite la plus marquante ? »

- Stéphane Michaux, [Science, droit, religion : Trois contes sur les deux natures](https://www.persee.fr/docAsPDF/roman_0048-8593_1976_num_6_13_5050.pdf)  

> "Pourtant les représentations archaïques que cette période retrouve s'avouent rarement telles. Elles s'abritent derrière l'autorité de disciplines ou d'institutions respectées : la médecine, la religion, le droit. Consciemment ou non, ces trois domaines remplissent la même fonction, celle d'alimenter la société des clichés anti-féministes qu'elle réclame, de déguiser l'ancien sous du neuf. Le progrès des sciences, l'évolution de la piété et de la théologie, le changement de la société sont autant de couvertures pour masquer un retour en arrière, pour imposer une vérité de contrebande qui s'insinue ensuite sans peine dans toutes les consciences."

- Francis Dupuis-Déri, [Le discours de la « crise de la masculinité » comme refus de l’égalité entre les sexes : histoire d’une rhétorique antiféministe](https://www.erudit.org/fr/revues/rf/2012-v25-n1-rf0153/1011118ar/)

> Le discours de la « crise de la masculinité » est aujourd’hui un lieu commun. Or cette prétendue crise de la masculinité survient alors que les hommes ont encore, en général, plus de pouvoir et de privilèges que les femmes. Il importe donc d’étudier, dans une perspective critique, la rhétorique de la « crise de la masculinité » pour évaluer le sens politique de ce discours. Ainsi, un retour dans l’histoire permet de constater qu’en Occident les hommes se prétendent en crise depuis au moins les cinq derniers siècles. Après avoir discuté plus précisément de trois périodes (le XVIIe siècle en Angleterre, la Révolution de 1789 en France et la fin du XIXe siècle et le début du XXe siècle en Occident), l’auteur accorde une attention particulière au discours contemporain. Son analyse permet de confirmer que ce discours est porteur d’une critique du féminisme et d’un refus de l’égalité entre les sexes. Ce discours sert aussi à justifier la (ré)affirmation d’une masculinité conventionnelle.

Sur [Fumigène](https://www.fumigene.org) :

- [Le féminisme n'est pas une option](https://www.fumigene.org/2020/03/12/le-feminisme-nest-pas-une-option/)
> Images de manifestations féministes.

- [La nuit des jeunes filles en feu](https://www.fumigene.org/2020/03/14/la-nuit-des-jeunes-filles-en-feu/)
> Tribune des colleuses.